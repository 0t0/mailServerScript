#! /bin/bash

# Install Haraka             line 10
# Bashrc                     line 19
# Server location            line 31
# Plugin config download     line 40
# Ldap plugin                line 85
# TLS plugin                 line 110

# Install haraka
npm i Haraka

# Variables
nl=$'\n'
echo "Default to prompt is 'y'. Empty inputs are also 'y'"
echo $nl
############################################################################################################

# Bashrc
haraka=$(pwd)/node_modules/Haraka/bin/haraka
read -p "Add haraka alias to bashrc?[y/n]: " -e -i "" ifal
if [ "$ifal" = "y" -o "$ifal" = "" ]; then
				echo "alias haraka=$haraka" >> ~/.bashrc
				source ~/.bashrc
else
				echo "Invalid input"
fi
echo $nl
############################################################################################################

# Server location
echo "Enter the directory to set up the server.
Add full path to install in a different location."
read -p "Enter server directory name: " dirname
config="$dirname/config"
$haraka -i $dirname
echo $nl
############################################################################################################

# Add plugin config file
echo "Download config files for given plugins
Haraka -l will show the available plugins
Update $config/plugins file accordingly
Exit with empty string"

while
				read -p "Plugin name: " plugname
				status=$(curl -s --head -w %{http_code} https://raw.githubusercontent.com/haraka/Haraka/master/config/$plugname.ini -o /dev/null)
				echo "Do check plugins at "https://haraka.github.io" and update config files accordingly"
				if [ "$status" = "200" -a "$plugname" != "" ]; then
								wget https://raw.githubusercontent.com/haraka/Haraka/master/config/$plugname.ini -P $config/
				elif [ "$status" = "404" -a "$plugname" != "" ]; then
								echo "Please check plugins at "https://haraka.github.io/""
								continue
				else
								echo "Exit"
								break
				fi
				if [ "$plugname" = "" ]; then
								echo "Exit"
				fi
				[[ "$plugname" != "" ]]
do true; done
echo $nl
echo "Some plugin files are not found in haraka git page so you can add
.ini files here and configure accordingly. Input the plugin name and the
file will be added at $config.
Exit with empty string"
read -p "Do you wish to touch .ini files?[y/n]: " -e -i "" iftouch
while [ "$iftouch" = "" -o "$iftouch" = "y" ]
do
				read -p "Plugin name: " plugname 
				if [ "$plugname" = "" ]; then
								echo "Exit"
								break

				else
								touch $config/$plugname.ini
								echo "Added $plugname.ini to $config"
				fi
done
echo $nl
############################################################################################################

# Ldap plugin
read -p "Add ldap plugin?[y/n]: " -e -i "" ifldap
if [ "$ifldap" = "y" -o "$ifldap" = "" ]; then
				npm i haraka-plugin-ldap
				$nl
				echo "Configuration options can be found in "https://github.com/tasansga/haraka-ldap""
				read -p "Do you wish to create a minimal config file with this script or download full file?[y/n]: " -e -i "" conf
				if [ "$conf" = "y" or "$conf" = "" ]; then
								read -p "Ldap server: " -e -i "localhost:389" server	
								read -p "Base dn: " -e -i "ou=mail,dc=example,dc=com" basedn
								read -p "Authentication dn/ user dn: " -e -i "uid=%u,$basedn" authdn
								read -p "ObjectClass for search filter: " -e -i "inetOrgPerson" fillClass
								read -p "Additional search parameter: " -e -i "uid=%u" fillAdd
								echo "server[]=$server $nl basedn=$basedn $nl $nl [authn] $nl dn=$authdn $nl searchFilter=(&($fillClass)($fillAdd)" > $config/ldap.ini
				else
								wget https://raw.githubusercontent.com/tasansga/haraka-ldap/master/config/ldap.ini -P $config/
				fi
				echo "ldap" >> $config/plugins
				echo "Ldap plugin added"
else
				echo "haraka-plugin-ldap not used"
fi
echo $nl
############################################################################################################

# TLS plugin
read -p "Add TLS plugin?[y/n]: " -e -i "" iftls
if [ "$iftls" = "y" -o "$iftls" = "" ]; then
				read -p "Certbot certifcate?[y/n]: " -e -i "" ifcert
				if [ "$ifcert" = "y" -o "$ifcert" = "" ]; then
								read -p "Domain name" domain
								certbotfile="/etc/letsencrypt/live"
								echo $certbotdir
								echo "$certbotdir/$domain/fullchain.pem $nl key=$certbotdir/$domain/privkey" > $config/tls.ini
				else
								read -p "Directory of certificate and key: " ifdir
								read -p "Certificate file name" cert
								read -p "Key file name" key
								echo "cert=$ifdir/$cert $nl key=$ifdir/$key"
				fi
else
				echo "TLS plugin not used. Bad move FYI"
				fi
############################################################################################################
