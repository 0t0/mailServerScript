#! /bin/sh
##################################################################################
#dovecot.conf      line 28
#mail.conf         line 43
#auth.conf         line 54
#master.con        line 62
#ssl.conf          line 81
#dovecot-ldap.conf line 91
#logging.conf      line 127
##################################################################################

##################################################################################
# dovecot debian packages
apt install dovecot-imapd dovecot-lmtpd dovecot-ldap
##################################################################################
#directories and files
dovHome=/etc/dovecot
mkdir -p $dovHome/orig
cp $dovHome/dovecot.conf $dovHome/orig/dovecot.conf.orig
cp $dovHome/conf.d/10-mail.conf $dovHome/orig/conf.d/10-mail.conf.orig
cp $dovHome/conf.d/10-auth.conf $dovHome/orig/conf.d/10-auth.conf.orig
cp $dovHome/conf.d/10-master.conf $dovHome/orig/conf.d/10-master.conf.orig
cp $dovHome/conf.d/10-logging.conf $dovHome/orig/conf.d/10-loggin.conf.orig
cp $dovHome/dovecot-ldap.conf.ext $dovHome/orig/dovecot-ldap.conf.ext.orig
##################################################################################
#Mail user group
mkdir /home/vmail
groupadd -g 3000 vmail
useradd -u 3000 -g vmail -d /home/vmail -s /bin/true vmail
chown -R vmail:vmail /home/vmail
##################################################################################
#dovecot.conf
dovecot_conf="
\n protocols = imap lmtp
\n listen = *, ::
\n instance_name = dovecot
\n login_greeting = Dovecot Ready
\n #protocols and confs
\n !include conf.d/*.conf
\n !include_try local.conf
\n !include_try /usr/share/dovecot/protocols.d/*.protocol
"
echo $dovecot_conf > $dovHome/dovecot.conf
##################################################################################
#10-mail.conf
mail_conf="
\n mail_location = maildir:/home/vmail/%n/Maildir/
\n namespace inbox {
\n   inbox = yes 
\n } 
\n mail_uid = vmail 
\n mail_gid = vmail  
"
echo $mail_conf > $dovHome/conf.d/10-mail.conf
##################################################################################
#10-auth.conf
auth_conf="
\n auth_username_chars = abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890.-_@ \n
\n auth_mechanism = plain login
\n !include auth-ldap.conf.ext"

echo $auth_conf > $dovHome/conf.d/10-auth.conf
##################################################################################
#10-master.conf
master_conf="
\nservice imap-login {
\n  inet_listener imap {
\n    port = 143
\n		ssl = yes
\n   }
\n }
\n
\n service lmtp{ 
\n   inet_listener lmtp {
\n     address = localhost
\n     port = 24
\n   }
\n }
"
echo $master_conf > $dovHome/conf.d/10-master.conf
##################################################################################
#10-ssl.conf
openssl dhparam -out $dovHome/dh.pem 4096
ssl_conf="
\n ssl = yes
\n ssl_cert = </etc/letsencrypt/live/$DOMAIN/fullchain.pem
\n ssl_cert = </etc/letsencrypt/live/$DOMAIN/privkey.pem
\n ssl_dh = </etc/dovecot/dh.pem"

echo ssl_conf > $dovHome/conf.d/10-ssl.conf
##################################################################################
#dovecot-ldap.conf
read -p "ldap port: " port
read -p "ObjectClass for ldap search: " mailUserClass
read -p "Admin dn: " adminDn
read -p "Admin Password: " adminPass
read -p "Base dn for search: " baseDn
ldap_conf="
\n # This use case of ldap has user in format uid=%u,ou=mail,dc=a,dc=com. Do change things based on individual preferences.
\n 
\n host = 127.0.0.1:$port
\n 
\n dn = $adminDn 
\n dnpass = $adminPass
\n 
\n sasl_bind = no
\n 
\n base = $baseDn
\n 
\n # subtree/onlevel/base
\n scope = subtree
\n 
\n # Use authentication binding for verifying password's validity
\n auth_bind = yes
\n auth_bind_userdn = uid=%u,$baseDn
\n 
\n #   %u - username
\n #   %n - user part in user@domain, same as %u if there's no domain
\n #   %d - domain part in user@domain, empty if user there's no domain
\n user_filter = (&(objectClass=$mailUserClass)(uid=%u))
\n 
\n #Do change accordingly
\n pass_attrs = uid=user,userPassword=password
"
echo $ldap_conf > $dovHome/dovecot-ldap.conf.ext
##################################################################################
#10-loggin.conf
read -p "Location of dovecot log: " logloc
mkdir -p $logloc
log_conf="
\n info_log_path = $logloc/info.log
\n debug_log_path = $logloc/debug.log
\n log_timestamp = "%b %d %H:%M:%S"

echo $log_conf > $dovHome/conf.d/10-logging.conf
